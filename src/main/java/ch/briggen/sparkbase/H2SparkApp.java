package ch.briggen.sparkbase;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.h2.tools.RunScript;
import org.h2.tools.Server;
import org.h2.jdbcx.JdbcDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class H2SparkApp extends JdbcSparkApp {

	final static Logger log = LoggerFactory.getLogger(H2SparkApp.class);
	
	private JdbcDataSource ds = null;
	protected DataSource getDataSource() {
		return ds;
	}
	
	protected String getDBConnectionString() {
		return "jdbc:h2:~/milklist-db;TRACE_LEVEL_SYSTEM_OUT=2";
	}

	protected String getDBUser() {
		return "sa";
	}

	protected String getDBPassword() {
		return "";
	}
	
	
	@Override
	public Connection getJdbcConnection() throws SQLException {
		if(log.isDebugEnabled()) {
			log.debug(ds.getDescription());
		}
		return ds.getConnection();
	}

	@Override
	protected void doInitializeDBServer() throws ConfigurationException {
		try {
			ds = new JdbcDataSource();
			ds.setURL(getDBConnectionString());
			ds.setUser(getDBUser());
			ds.setPassword(getDBPassword());
			
			// Test connection
			try(Connection c = ds.getConnection()){
				if(!c.isValid(1)){
					log.error("Driver not initialized. Test connection is not valid ");
					log.debug(ds.getDescription());
					throw new ConfigurationException("Driver not initialized. Test connection is not valid ");
				}
			}
			
			log.debug("DB connection initialized and valid.");	
		} catch (SQLException e) {
			log.error("Driver not initialized. " + e.getMessage());
			log.debug(ds.getDescription());
			throw new ConfigurationException("Driver not initialized.", e);
		}

		startH2Console();

	}

	private void startH2Console() throws ConfigurationException {
		try {
			Server.createWebServer().start();
			log.info("H2 Console started on http://localhost:8082");
		} catch (SQLException sqle) {
			log.error("H2 Console Not Started " + sqle.getMessage());
			throw new ConfigurationException("H2 Console Not Started", sqle);
		}
	}
	
	@Override
	protected void doInitializeDB() throws ConfigurationException {
		ClassLoader myLoader = this.getClass().getClassLoader();
		InputStream sqlscript = myLoader.getResourceAsStream("initdatabase.sql");
		if(null == sqlscript)
		{
			log.info("no database initialization found at src/main/resources/initdatabase.sql");
		}
		else
		{
			log.info("initializing database from src/main/resources/initdatabase.sql ...");
			try {
				RunScript.execute(getJdbcConnection(),new InputStreamReader(sqlscript));
			} catch (SQLException e) {
				log.error("SQL error during db initializiation. " + e.getMessage());
				throw new ConfigurationException("SQL error during db initializiation.", e);
			}
		}
		 
	}
   

}
