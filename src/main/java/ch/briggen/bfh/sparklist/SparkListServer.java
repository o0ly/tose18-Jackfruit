package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.ProjektDeleteController;
import ch.briggen.bfh.sparklist.web.ProjektEditController;
import ch.briggen.bfh.sparklist.web.ProjektNewController;
import ch.briggen.bfh.sparklist.web.ProjektUpdateController;
import ch.briggen.bfh.sparklist.web.MitarbeiterDeleteController;
import ch.briggen.bfh.sparklist.web.MitarbeiterEditController;
import ch.briggen.bfh.sparklist.web.MitarbeiterNewController;
import ch.briggen.bfh.sparklist.web.MitarbeiterUpdateController;
import ch.briggen.bfh.sparklist.web.StakeholderlistDeleteController;
import ch.briggen.bfh.sparklist.web.StakeholderlistEditController;
import ch.briggen.bfh.sparklist.web.StakeholderlistNewController;
import ch.briggen.bfh.sparklist.web.StakeholderlistUpdateController;
import ch.briggen.bfh.sparklist.web.StakeholderlistanzeigenDeleteController;
import ch.briggen.bfh.sparklist.web.StakeholderlistanzeigenEditController;
import ch.briggen.bfh.sparklist.web.StakeholderlistanzeigenNewController;
import ch.briggen.bfh.sparklist.web.StakeholderlistanzeigenUpdateController;
import ch.briggen.bfh.sparklist.web.MeilensteinDeleteController;
import ch.briggen.bfh.sparklist.web.MeilensteinEditController;
import ch.briggen.bfh.sparklist.web.MeilensteinNewController;
import ch.briggen.bfh.sparklist.web.MeilensteinUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		
		get("/stakeholderlist", new StakeholderlistEditController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholderlist/update", new StakeholderlistUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholderlist/delete", new StakeholderlistDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholderlist/new", new StakeholderlistNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/stakeholderlistanzeigen", new StakeholderlistanzeigenEditController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholderlistanzeigen/update", new StakeholderlistanzeigenUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholderlistanzeigen/delete", new StakeholderlistanzeigenDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholderlistanzeigen/new", new StakeholderlistanzeigenNewController(), new UTF8ThymeleafTemplateEngine());

		get("/projekt", new ProjektEditController(), new UTF8ThymeleafTemplateEngine());
		post("/projekt/update", new ProjektUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/projekt/delete", new ProjektDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/projekt/new", new ProjektNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/mitarbeiter", new MitarbeiterEditController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/update", new MitarbeiterUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/delete", new MitarbeiterDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/new", new MitarbeiterNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/meilenstein", new MeilensteinEditController(), new UTF8ThymeleafTemplateEngine());
		post("/meilenstein/update", new MeilensteinUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/meilenstein/delete", new MeilensteinDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/meilenstein/new", new MeilensteinNewController(), new UTF8ThymeleafTemplateEngine());
	}

}
