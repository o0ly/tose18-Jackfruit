package ch.briggen.bfh.sparklist.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

// Einträge in der Liste mit allen Attribute (inkl. einer eindeutigen ID)
public class Projekt {
	
	private long id;
	private String name;
	private String beschreibung;
	private String ziele;
	private String wichtig;
	private String start;
	private String ende;
	private Collection<Mitarbeiter> mitarbeiter = new ArrayList<>();
	private Collection<Meilenstein> meilenstein = new ArrayList<>();
	
	//Defaulkonstruktor für die Verwendung in einem Controller
	public Projekt()
	{
		
	}
	
	//Konstruktor der einzelnen Attribute (@param x)
	public Projekt(long id, String name, String beschreibung, String ziele, String wichtig, String start, String ende,Collection<Mitarbeiter> mitarbeiter, Collection<Meilenstein> meilenstein)
	{
		this.id = id;
		this.name = name;
		this.beschreibung = beschreibung;
		this.ziele = ziele;
		this.wichtig = wichtig;
		this.start = start;
		this.ende = ende;
		this.mitarbeiter = mitarbeiter;
		this.meilenstein = meilenstein;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getZiele() {
		return ziele;
	}

	public void setZiele(String ziele) {
		this.ziele = ziele;
	}
	
	public String getWichtig() {
		return wichtig;
	}

	public void setWichtig(String wichtig) {
		this.wichtig = wichtig;
	}
	
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
	
	public String getEnde() {
		return ende;
	}

	public void setEnde(String ende) {
		this.ende = ende;
	}
	
	public Collection<Mitarbeiter> getMitarbeiter() {
		return mitarbeiter;
		
	}

	public void setMitarbeiter(List<Mitarbeiter> mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
		
	}
	

	public Collection<Meilenstein> getMeilenstein() {
		return meilenstein;
			}

	public void setMeilenstein(List<Meilenstein> meilenstein) {
		this.meilenstein= meilenstein;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public ArrayList<String> getMitarbeitername(){
		ArrayList<String> list = new ArrayList<String>();
		Iterator<Mitarbeiter> iter = mitarbeiter.iterator();
		while (iter.hasNext()) {
			list.add(iter.next().getName());
		}
			
		return list;
	}
	public ArrayList<String> getMeilensteintitel(){
		ArrayList <String> list = new ArrayList<String>();
		Iterator<Meilenstein> iter = meilenstein.iterator();	
		while (iter.hasNext()) {
				list.add(iter.next().getTitel());
		}
		return list;
	}
	@Override
	public String toString() {
		String mitarbeiterString = "";
		String meilensteinString ="";
		return String.format("Projekt:{id: %d; name: %s; beschreibung: %s; ziele: %s; wichtig: %s; start: %s; ende: %s; mitarbeiter: %s; meilenstein: %s;}", id, name, beschreibung, ziele, wichtig, start, ende, mitarbeiterString, meilensteinString);
	}
}
