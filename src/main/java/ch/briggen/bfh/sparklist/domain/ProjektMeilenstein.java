package ch.briggen.bfh.sparklist.domain;

public class ProjektMeilenstein {
	
	private long id;
	private long projekt_id;
	private long meilenstein_id;
	
	public ProjektMeilenstein()
	{
		
	}
	public ProjektMeilenstein(long id, long meilenstein_id,long projekt_id)
	{
		this.id = id;
		this.meilenstein_id = meilenstein_id;
		this.projekt_id = projekt_id;
	}

	public long getMeilenstein_id() {
		return meilenstein_id;
	}

	public void setMeilenstein_id(long meilenstein_id) {
		this.meilenstein_id = meilenstein_id;
	}
	
	public long getProjekt_id() {
		return projekt_id;
	}

	public void setProjekt_id(long projekt_id) {
		this.projekt_id = projekt_id;
	}
	
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("ProjektMeilenstein:{id: %d; meilenstein_id: %d; projekt_id: %d;}", id, meilenstein_id, projekt_id);
	}
	

}
