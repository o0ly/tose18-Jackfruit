package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjektMitarbeiterRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjektMitarbeiterRepository.class);

	public Collection<ProjektMitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, mitarbeiter_id, projekt_id from projektmitarbeiter");
			ResultSet rs = stmt.executeQuery();
			return mapProjektMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projektmitarbeiter. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	// wenn beide anzeigen lassen nochmal nur mit wheere mitarbeiter id
	public ProjektMitarbeiter getById(long id) {
		log.trace("getById " + id);
		
	
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, mitarbeiter_id, projekt_id from projektmitarbeiter where projekt_id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektMitarbeiter(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projektmitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	public void save(ProjektMitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projektmitarbeiter set mitarbeiter_id=?, projekt_id=? where id=?");
			stmt.setLong(1, i.getMitarbeiter_id());
			stmt.setLong(2, i.getProjekt_id());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projektmitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektmitarbeiter where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projektmitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
	
	public void deletebyprojektid(long projektid) {
		log.trace("delete " + projektid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektmitarbeiter where projekt_id=?");
			stmt.setLong(1, projektid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projektmitarbeiter by id " + projektid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
	
	
	public long insert(ProjektMitarbeiter i) {
		
		log.trace("insert " + i);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projektmitarbeiter (mitarbeiter_id, projekt_id) values (?,?)");
			stmt.setLong(1, i.getMitarbeiter_id());
			stmt.setLong(2, i.getProjekt_id());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projektmitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	private static Collection<ProjektMitarbeiter> mapProjektMitarbeiter(ResultSet rs) throws SQLException 
	{
		LinkedList<ProjektMitarbeiter> pmlist = new LinkedList<ProjektMitarbeiter>();
		while(rs.next())
		{
			ProjektMitarbeiter i = new ProjektMitarbeiter(rs.getLong("id"),rs.getLong("mitarbeiter_id"),rs.getLong("projekt_id"));
			pmlist.add(i);
		}
		return pmlist;
	}

}
