package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MitarbeiterRepository {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterRepository.class);
	

	public Collection<Mitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, rolle, arbeit, name from mitarbeiter");
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all mitarbeiter. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public Collection<Mitarbeiter> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, rolle, arbeit name from mitarbeiter where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving mitarbeiter by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	public Mitarbeiter getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, rolle, arbeit, name from mitarbeiter where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
		
	public Collection<Mitarbeiter> getByProjekId(long projektid) {
		log.trace("getByProjektId " + projektid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from mitarbeiter as m "
					+ "inner join projektmitarbeiter as pm on m.id = pm.mitarbeiter_id "
					+ "where pm.projekt_id=?");
			stmt.setLong(1, projektid);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving mitarbeiter by name " + projektid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	public void save(Mitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update mitarbeiter set name=?, vorname=?, rolle=?, arbeit=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getRolle());
			stmt.setLong(4, i.getArbeit());
			stmt.setLong(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from mitarbeiter where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	public long insert(Mitarbeiter i) {
		
		log.trace("insert " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into mitarbeiter (name, vorname, rolle, arbeit) values (?,?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getRolle());
			stmt.setLong(4, i.getArbeit());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	private static Collection<Mitarbeiter> mapMitarbeiter(ResultSet rs) throws SQLException 
	{
		LinkedList<Mitarbeiter> mlist = new LinkedList<Mitarbeiter>();
		while(rs.next())
		{
			Mitarbeiter i = new Mitarbeiter(rs.getLong("id"),rs.getString("name"),rs.getString("vorname"), rs.getString("rolle"),rs.getLong("arbeit"));
			mlist.add(i);
		}
		return mlist;
	}

}
