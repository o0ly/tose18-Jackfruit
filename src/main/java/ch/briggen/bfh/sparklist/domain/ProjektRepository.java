package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;
//Repository für alle Projekte
//Hier werden alle DB-Abfragen von der Tabelle Projekts implementiert 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjektRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjektRepository.class);

	//Liefert alle projekts in der DB, Collection aller Projekte 
	public Collection<Projekt> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from projekts");
			ResultSet rs = stmt.executeQuery();
			return mapProjekts(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projekts. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	//Liefert das Projekt mit der übergebenen ID
	//param id, id des Projektes
	//return Projekt oder Null
	public Projekt getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, beschreibung, ziele, wichtig, start, ende from projekts where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjekts(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projekts by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	//Speichert das übergebene Projekt in der DB (Update)
	//1-7 Reihenfolge, wie DB Eingaben in JAVA übergeben wird
	//param i
	public void save(Projekt i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projekts set name=?, beschreibung=?, ziele=?, wichtig=?, start=?, ende=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getZiele());
			stmt.setString(4, i.getWichtig());
			stmt.setString(5, i.getStart());
			stmt.setString(6, i.getEnde());
			stmt.setLong(7, i.getId());
			stmt.executeUpdate();

			//Fremdschlüssel ID, zuerste werden alle einträge gelöst, Funktion deleteby... befindet sich in Projektmitarbeiterrepository
			ProjektMitarbeiterRepository pm = new ProjektMitarbeiterRepository();
			pm.deletebyprojektid(i.getId());
			for(Mitarbeiter m : i.getMitarbeiter()) {
				pm.insert(new ProjektMitarbeiter(0, m.getId(), i.getId()));
			}
			
			ProjektMeilensteinRepository pme = new ProjektMeilensteinRepository();
			pme.deletebyprojektidm(i.getId());
			for(Meilenstein me : i.getMeilenstein()) {
				pme.insert(new ProjektMeilenstein(0, me.getId(), i.getId()));
			}
			
			
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projekt " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	//Löst das Projekt mit der angebenen ID aus der DB
	//Param id Projekt ID
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projekts where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projekts by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
	//Speichert das angegebene Projekt in er DB
	//Parameter p neu zu erstellendes Projekt
	//return Liefert die von der DB generierte id des neuen Projekts zurück
	public long insert(Projekt p) {
		
		log.trace("insert " + p);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projekts (name, beschreibung, ziele, wichtig, start, ende) values (?,?,?,?,?,?)");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getBeschreibung());
			stmt.setString(3, p.getZiele());
			stmt.setString(4, p.getWichtig());
			stmt.setString(5, p.getStart());
			stmt.setString(6, p.getEnde());
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long pid = key.getLong(1);
			
			//Fremdschlüssel, wie oben
			ProjektMitarbeiterRepository pm = new ProjektMitarbeiterRepository();
			pm.deletebyprojektid(pid);
			for(Mitarbeiter m : p.getMitarbeiter()) {
				pm.insert(new ProjektMitarbeiter(0, m.getId(), pid));}
				
			ProjektMeilensteinRepository pme = new ProjektMeilensteinRepository();
			pme.deletebyprojektidm(pid);
			for(Meilenstein me : p.getMeilenstein()) {
			pme.insert(new ProjektMeilenstein(0, me.getId(), pid));}
			
			
			return pid;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projekt " + p;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	//Helfer zum konvertieren der Resultsets in Projekt-Objekte
	private static Collection<Projekt> mapProjekts(ResultSet rs) throws SQLException 
	{
		LinkedList<Projekt> plist = new LinkedList<Projekt>();
		while(rs.next())
		{
			long id = rs.getLong("id");
			
			//Fremdschlüssel
			MitarbeiterRepository pm = new MitarbeiterRepository();
			MeilensteinRepository pme = new MeilensteinRepository();
			
		
 			Projekt i = new Projekt(rs.getLong("id"),rs.getString("name"),rs.getString("beschreibung"),rs.getString("ziele"),rs.getString("wichtig"),rs.getString("start"),rs.getString("ende"), pm.getByProjekId(id), pme.getByProjekIdm(id));
			plist.add(i);
		}
		return plist;
	}

}
