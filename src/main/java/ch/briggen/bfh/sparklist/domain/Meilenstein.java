package ch.briggen.bfh.sparklist.domain;


public class Meilenstein {
	
	private long id;
	private String titel;
	private String beschreibung;
	private String istdatum;
	private String solldatum;
	private long istkosten;
	private long sollkosten;

	
	public Meilenstein()
	{
		
	}
	
	public Meilenstein(long id, String titel, String beschreibung, String istdatum, String solldatum, long istkosten, long sollkosten)
	{
		this.id = id;
		this.titel = titel;
		this.beschreibung = beschreibung;
		this.istdatum = istdatum;
		this.solldatum = solldatum;
		this.istkosten = istkosten;
		this.sollkosten = sollkosten;

	}

	
	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getSolldatum() {
		return solldatum;
	}

	public void setSolldatum(String solldatum) {
		this.solldatum = solldatum;
	}
	
	public String getIstdatum() {
		return istdatum;
	}

	public void setIstdatum(String istdatum) {
		this.istdatum = istdatum;
	}
	
	public long getSollkosten() {
		return sollkosten;
	}

	public void setSollkosten(long sollkosten) {
		this.sollkosten = sollkosten;
	}
	
	public long getIstkosten() {
		return istkosten;
	}

	public void setIstkosten(long istkosten) {
		this.istkosten = istkosten;
	}
	
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Meilensteine:{id: %d; titel: %s; beschreibung: %s; solldatum: %s; istdatum: %s; sollkosten: %d; istkosten: %d;}", id, titel, beschreibung, solldatum, istdatum, sollkosten, istkosten);
	}
	

}
