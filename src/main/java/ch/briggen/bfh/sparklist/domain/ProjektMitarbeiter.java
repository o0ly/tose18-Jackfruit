package ch.briggen.bfh.sparklist.domain;

public class ProjektMitarbeiter {
	
	private long id;
	private long mitarbeiter_id;
	private long projekt_id;
	
	public ProjektMitarbeiter()
	{
		
	}
	public ProjektMitarbeiter(long id, long mitarbeiter_id, long projekt_id)
	{
		this.id = id;
		this.mitarbeiter_id = mitarbeiter_id;
		this.projekt_id = projekt_id;
	}

	
	public long getMitarbeiter_id() {
		return mitarbeiter_id;
	}

	public void setMitarbeiter_id(long mitarbeiter_id) {
		this.mitarbeiter_id = mitarbeiter_id;
	}

	public long getProjekt_id() {
		return projekt_id;
	}

	public void setProjekt_id(long projekt_id) {
		this.projekt_id = projekt_id;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("ProjektMitarbeiter:{id: %d; mitarbeiter_id: %d; projekt_id: %d;}", id, mitarbeiter_id, projekt_id);
	}
	

}
