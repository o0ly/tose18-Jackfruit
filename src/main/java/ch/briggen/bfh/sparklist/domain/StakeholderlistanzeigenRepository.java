package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Items. 
 * Hier werden alle Funktionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggenjavascript:ins('STAKEHOLDER',true)
 *
 */


public class StakeholderlistanzeigenRepository {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderlistanzeigenRepository.class);
	

	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Stakeholderlistanzeigen> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, firma, rolle, wichtig from stakeholder");
			ResultSet rs = stmt.executeQuery();
			return mapStakeholderlistanzeigen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all stakeholder. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Items mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Stakeholderlistanzeigen> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, firma, rolle, wichtig from stakeholder where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholderlistanzeigen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving stakeholder by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Stakeholderlistanzeigen getById(long id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, vorname, firma, rolle, wichtig from stakeholder where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholderlistanzeigen(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving stakeholder by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Stakeholderlistanzeigen i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update stakeholder set name=?, vorname=?, firma=?, rolle=?, wichtig=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getFirma());
			stmt.setString(4, i.getRolle());
			stmt.setString(5, i.getWichtig());
			stmt.setLong(6, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating items " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from stakeholder where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing stakeholder by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Item in der DB. INSERT.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public long insert(Stakeholderlistanzeigen i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into stakeholder (name, vorname, firma, rolle, wichtig) values (?,?,?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getFirma());
			stmt.setString(4, i.getRolle());
			stmt.setString(5, i.getWichtig());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating stakeholder " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Stakeholderlistanzeigen> mapStakeholderlistanzeigen(ResultSet rs) throws SQLException 
	{
		LinkedList<Stakeholderlistanzeigen> slist = new LinkedList<Stakeholderlistanzeigen>();
		while(rs.next())
		{
			Stakeholderlistanzeigen i = new Stakeholderlistanzeigen(rs.getLong("id"),rs.getString("name"),rs.getString("vorname"), rs.getString("firma"),rs.getString("rolle"),rs.getString("wichtig") );
			slist.add(i);
		}
		return slist;
	}

}
