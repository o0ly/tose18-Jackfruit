package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MeilensteinRepository {
	
	private final Logger log = LoggerFactory.getLogger(MeilensteinRepository.class);
	
	
	public Collection<Meilenstein> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, titel, beschreibung, solldatum, istdatum, sollkosten, istkosten from meilenstein");
			ResultSet rs = stmt.executeQuery();
			return mapMeilenstein(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all meilensteine. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}


	public Collection<Meilenstein> getByTitel(String titel) {
		log.trace("getByTitel " + titel);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from meilenstein where titel=?");
			stmt.setString(1, titel);
			ResultSet rs = stmt.executeQuery();
			return mapMeilenstein(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving meilenstein by titel " + titel;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
		}
		
	public Meilenstein getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, titel, beschreibung, solldatum, istdatum, sollkosten, istkosten from meilenstein where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapMeilenstein(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving meilenstein by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	public Collection<Meilenstein> getByProjekIdm(long projektid) {
		log.trace("getByProjektIdm " + projektid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from meilenstein as me "
					+ "inner join projektmeilenstein as pme on me.id = pme.meilenstein_id "
					+ "where pme.projekt_id=?");
			stmt.setLong(1, projektid);
			ResultSet rs = stmt.executeQuery();
			return mapMeilenstein(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving meilenstein by titel " + projektid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}
	public void save(Meilenstein i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update meilenstein set titel=?, beschreibung=?, solldatum=?, istdatum=?, sollkosten=?, istkosten=? where id=?");
			stmt.setString(1, i.getTitel());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getSolldatum());
			stmt.setString(4, i.getIstdatum());
			stmt.setLong(5, i.getSollkosten());
			stmt.setLong(6, i.getIstkosten());
			stmt.setLong(7, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating meilenstein " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from meilenstein where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing meilenstein by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	public long insert(Meilenstein i) {
		
		log.trace("insert " + i);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into meilenstein (titel, beschreibung, solldatum, istdatum, sollkosten, istkosten) values (?,?,?,?,?,?)");
			stmt.setString(1, i.getTitel());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getSolldatum());
			stmt.setString(4, i.getIstdatum());
			stmt.setLong(5, i.getSollkosten());
			stmt.setLong(6, i.getIstkosten());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating meilenstein " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	private static Collection<Meilenstein> mapMeilenstein(ResultSet rs) throws SQLException 
	{
		LinkedList<Meilenstein> melist = new LinkedList<Meilenstein>();
		while(rs.next())
		{
	
			Meilenstein i = new Meilenstein(rs.getLong("id"),rs.getString("titel"),rs.getString("beschreibung"),rs.getString("solldatum"),rs.getString("istdatum"),rs.getLong("sollkosten"),rs.getLong("istkosten"));
			melist.add(i);
		}
		return melist;
	}
	
	

}
