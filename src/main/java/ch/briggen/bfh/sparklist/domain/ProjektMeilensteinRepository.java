package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjektMeilensteinRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjektMeilensteinRepository.class);

	public Collection<ProjektMeilenstein> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, meilenstein_id, projekt_id  from projektmeilenstein");
			ResultSet rs = stmt.executeQuery();
			return mapProjektMeilenstein(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projektmeilenstein. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	// wenn beide anzeigen lassen nochmal nur mit wheere meilenstein id
	public ProjektMeilenstein getById(long id) {
		log.trace("getById " + id);
		
			try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, meilenstein_id, projekt_id from projektmeilenstein where projekt_id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektMeilenstein(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projektmeilenstein by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	public void save(ProjektMeilenstein i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projektmeilenstein set  meilenstein_id=?, pojekt_id=? where id=?");
			stmt.setLong(1, i.getProjekt_id());
			stmt.setLong(2, i.getMeilenstein_id());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projektmeilenstein" + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektmeilenstein where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projektmeilenstein by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}
	public void deletebyprojektidm(long projektid) {
		
		log.trace("delete " + projektid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektmeilenstein where projekt_id=?");
			stmt.setLong(1, projektid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projektmeilenstein by id " + projektid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	
	public long insert(ProjektMeilenstein i) {
		
		log.trace("insert " + i);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projektmeilenstein (meilenstein_id, projekt_id) values (?,?)");
			stmt.setLong(1, i.getMeilenstein_id());
			stmt.setLong(2, i.getProjekt_id());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projektmeilenstein " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	private static Collection<ProjektMeilenstein> mapProjektMeilenstein (ResultSet rs) throws SQLException 
	{
		LinkedList<ProjektMeilenstein> pmelist = new LinkedList<ProjektMeilenstein>();
		while(rs.next())
		{
			ProjektMeilenstein i = new ProjektMeilenstein(rs.getLong("id"), rs.getLong("meilenstein_id"), rs.getLong("projekt_id"));
			pmelist.add(i);
		}
		return pmelist;
	}

}
