package ch.briggen.bfh.sparklist.domain;

public class Mitarbeiter {
	
	private long id;
	private String name;
	private String vorname;
	private String rolle;
	private long arbeit;


	public Mitarbeiter()
	{
		
	}
	
	public Mitarbeiter(long id, String name, String vorname, String rolle, long arbeit)
	{
		this.id = id;
		this.name = name;
		this.vorname = vorname;
		this.rolle = rolle;
		this.arbeit = arbeit;		
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	
	public String getRolle() {
		return rolle;
	}

	public void setRolle(String rolle) {
		this.rolle = rolle;
	}
	
	public long getArbeit() {
		return arbeit;
	}

	public void setArbeit(long arbeit) {
		this.arbeit = arbeit;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Mitarbeiter:{id: %d; name: %s; vorname: %s; rolle: %s; arbeit: %d;}", id, name, vorname, rolle, arbeit);
	}
	

}
