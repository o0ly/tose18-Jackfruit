package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Stakeholderlist {
	
	private long id;
	private String name;
	private String vorname;
	private String firma;
	private String rolle;
	private String wichtig;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Stakeholderlist()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Stakeholderlist(long id, String name, String vorname, String firma, String rolle, String wichtig)
	{
		this.id = id;
		this.name = name;
		this.vorname = vorname;
		this.firma = firma;
		this.rolle = rolle;
		this.wichtig = wichtig;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}
	
	public String getRolle() {
		return rolle;
	}

	public void setRolle(String rolle) {
		this.rolle = rolle;
	}
	
	public String getWichtig() {
		return wichtig;
	}

	public void setWichtig(String wichtig) {
		this.wichtig = wichtig;
	}
	

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Stakeholder:{id: %d; name: %s; vorname: %s; firma: %s; rolle: %s; wichtig: %s;}", id, name, vorname, firma, rolle, wichtig);
	}
	

}
