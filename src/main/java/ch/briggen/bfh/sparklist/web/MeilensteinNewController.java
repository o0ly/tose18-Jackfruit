package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Meilenstein;
import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class MeilensteinNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MeilensteinNewController.class);
		
	private MeilensteinRepository meilensteinRepo = new MeilensteinRepository();
	
	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Meilenstein meilensteinDetail = MeilensteinWebHelper.meilensteinFromWeb(request);
		log.trace("POST /meilenstein/new mit meilensteinDetail " + meilensteinDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = meilensteinRepo.insert(meilensteinDetail);
		
		
		response.redirect("/meilenstein?id="+id);
		return null;
	}
}


