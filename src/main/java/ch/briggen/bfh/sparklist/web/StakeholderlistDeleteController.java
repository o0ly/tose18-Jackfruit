package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.StakeholderlistRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

public class StakeholderlistDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderlistDeleteController.class);
		
	private StakeholderlistRepository stakeholderlistRepo = new StakeholderlistRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /stakeholderlist/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		stakeholderlistRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}


