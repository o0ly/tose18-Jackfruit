package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Meilenstein;
import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class MeilensteinEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(MeilensteinEditController.class);
	
	private MeilensteinRepository meilensteinRepo = new MeilensteinRepository();

	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /meilenstein für INSERT mit id " + idString);
			model.put("postAction", "/meilenstein/new");
			model.put("meilensteinDetail", new Meilenstein());

		}
		else
		{
			log.trace("GET /meilenstein für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/meilenstein/update");
			
			
			Long id = Long.parseLong(idString);
			Meilenstein i = meilensteinRepo.getById(id);
			model.put("meilensteinDetail", i);
			
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "meilensteinDetailTemplate");
	}
	
	
	
}


