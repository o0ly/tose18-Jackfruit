package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Stakeholderlist;
import ch.briggen.bfh.sparklist.domain.StakeholderlistRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class StakeholderlistEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(StakeholderlistEditController.class);

	private StakeholderlistRepository stakeholderlistRepo = new StakeholderlistRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /stakeholderlist für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/stakeholderlist/new");
			model.put("stakeholderlistDetail", new Stakeholderlist());

		}
		else
		{
			log.trace("GET /stakeholderlist für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/stakeholderlist/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Stakeholderlist i = stakeholderlistRepo.getById(id);
			model.put("stakeholderlistDetail", i);
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "stakeholderlistDetailTemplate");
	}
	
	
	
}


