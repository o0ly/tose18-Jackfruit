package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import spark.Request;

class MitarbeiterWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MitarbeiterWebHelper.class);
	
	public static Mitarbeiter mitarbeiterFromWeb(Request request)
	{
		return new Mitarbeiter(
				Long.parseLong(request.queryParams("mitarbeiterDetail.id")),
				request.queryParams("mitarbeiterDetail.name"),
				request.queryParams("mitarbeiterDetail.vorname"),
				request.queryParams("mitarbeiterDetail.rolle"),
				Long.parseLong(request.queryParams("mitarbeiterDetail.arbeit")));
	}

}
