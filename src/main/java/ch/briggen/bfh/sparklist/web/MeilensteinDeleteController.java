package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

public class MeilensteinDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MeilensteinDeleteController.class);
		
	private MeilensteinRepository meilensteinRepo = new MeilensteinRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /meilenstein/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		meilensteinRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}


