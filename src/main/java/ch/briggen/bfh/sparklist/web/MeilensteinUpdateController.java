package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import ch.briggen.bfh.sparklist.domain.Meilenstein;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class MeilensteinUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(MeilensteinUpdateController.class);
		
	private MeilensteinRepository meilensteinRepo = new MeilensteinRepository();
	

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Meilenstein meilensteinDetail = MeilensteinWebHelper.meilensteinFromWeb(request);
		
		log.trace("POST /meilenstein/update mit meilensteinDetail " + meilensteinDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		meilensteinRepo.save(meilensteinDetail);
		response.redirect("/meilenstein?id="+meilensteinDetail.getId());
		return null;
	}
}


