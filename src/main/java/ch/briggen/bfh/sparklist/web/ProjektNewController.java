package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

public class ProjektNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektNewController.class);
		
	private ProjektRepository projektRepo = new ProjektRepository();
	
	//Erstellt ein neues Projekt in der DB. Die id wird von der DB erstellt.
	//Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=1  wenn die id 1 war.)
	//return Redirect zurück zur Detailmaske
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projekt projektDetail = ProjektWebHelper.projektFromWeb(request);
		log.trace("POST /projekt/new mit projektDetail " + projektDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = projektRepo.insert(projektDetail);
		response.redirect("/projekt?id="+id);
		return null;
	}
}


