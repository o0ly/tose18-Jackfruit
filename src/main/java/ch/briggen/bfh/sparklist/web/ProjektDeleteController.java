package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

//Controller für alle Optionen auf einzlne Items
public class ProjektDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektDeleteController.class);
		
	private ProjektRepository projektRepo = new ProjektRepository();
	
	//Löscht das rojekt mit der übergenen id in der DB
	// /item/delete&id=1 löscht das Item mit der Id 1 aus der DB
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /projekt/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		projektRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}


