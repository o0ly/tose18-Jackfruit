package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Meilenstein;
import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import spark.Request;

class ProjektWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjektWebHelper.class);
	
	public static Projekt projektFromWeb(Request request) {
			
				MitarbeiterRepository pm = new MitarbeiterRepository();
				ArrayList<Mitarbeiter> maListe = new ArrayList<>();
				
				MeilensteinRepository pme = new MeilensteinRepository();
				ArrayList<Meilenstein> meListe = new ArrayList<>();
				
				//Fremdschlüssel
				for(String maId: request.queryParamsValues("projektDetail.mitarbeiterid") ) {
					Mitarbeiter m = pm.getById(Long.parseLong(maId));
					maListe.add(m);}
				
				
				for(String meId: request.queryParamsValues("projektDetail.meilenstein_id") ) {
					Meilenstein me = pme.getById(Long.parseLong(meId));
					meListe.add(me);}
				
		return new Projekt(
				Long.parseLong(request.queryParams("projektDetail.id")),
				request.queryParams("projektDetail.name"),
				request.queryParams("projektDetail.beschreibung"),
				request.queryParams("projektDetail.ziele"),
				request.queryParams("projektDetail.wichtig"),
				request.queryParams("projektDetail.start"),
				request.queryParams("projektDetail.ende"),
				maListe, 
				meListe);

	}

}
