package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Meilenstein;
import spark.Request;

class MeilensteinWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MeilensteinWebHelper.class);
	
	public static Meilenstein meilensteinFromWeb(Request request) {
	{
		return new Meilenstein(
				Long.parseLong(request.queryParams("meilensteinDetail.id")),
				request.queryParams("meilensteinDetail.titel"),
				request.queryParams("meilensteinDetail.beschreibung"),
				request.queryParams("meilensteinDetail.solldatum"),
				request.queryParams("meilensteinDetail.istdatum"),
				Long.parseLong(request.queryParams("meilensteinDetail.sollkosten")),
				Long.parseLong(request.queryParams("meilensteinDetail.istkosten")));
	}

}
}
