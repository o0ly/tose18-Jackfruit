package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Stakeholderlist;
import ch.briggen.bfh.sparklist.domain.StakeholderlistRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class StakeholderlistUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderlistUpdateController.class);
		
	private StakeholderlistRepository stakeholderlistRepo = new StakeholderlistRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Stakeholderlist stakeholderlistDetail = StakeholderlistWebHelper.stakeholderlistFromWeb(request);
		
		log.trace("POST /stakholderlist/update mit stakeholderlistDetail " + stakeholderlistDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		stakeholderlistRepo.save(stakeholderlistDetail);
		response.redirect("/stakeholderlist?id="+stakeholderlistDetail.getId());
		return null;
	}
}


