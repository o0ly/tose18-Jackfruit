package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjektUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjektUpdateController.class);
		
	private ProjektRepository projektRepo = new ProjektRepository();
	
	//Schreibt das geänderte Projekt zurück in die Datenbank
	//Bei Erfolg erfolgt ein Redirect zurück auf die Detailseite /projekt mit der Projekt-id als Paremeter mit dem Namen id
	//Return edirect nach /projekt: via Browser wird /projekt aufgerufen, also editProjekt weiter oben und dann das Detailformular angezeigt. 
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projekt projektDetail = ProjektWebHelper.projektFromWeb(request);
		
		log.trace("POST /projekt/update mit projektDetail " + projektDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		projektRepo.save(projektDetail);
		response.redirect("/projekt?id="+projektDetail.getId());
		return null;
	}
}


