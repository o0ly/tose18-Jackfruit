package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Stakeholderlist;
import spark.Request;

class StakeholderlistWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(StakeholderlistWebHelper.class);
	
	public static Stakeholderlist stakeholderlistFromWeb(Request request)
	{
		return new Stakeholderlist(
				Long.parseLong(request.queryParams("stakeholderlistDetail.id")),
				request.queryParams("stakeholderlistDetail.name"),
				request.queryParams("stakeholderlistDetail.vorname"),
				request.queryParams("stakeholderlistDetail.firma"),
				request.queryParams("stakeholderlistDetail.rolle"),
				request.queryParams("stakeholderlistDetail.wichtig"));
	}

}
