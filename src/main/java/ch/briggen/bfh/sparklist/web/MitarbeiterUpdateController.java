package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;


public class MitarbeiterUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterUpdateController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter mitarbeiterDetail = MitarbeiterWebHelper.mitarbeiterFromWeb(request);
		
		log.trace("POST /mitarbeiter/update mit mitarbeiterDetail " + mitarbeiterDetail);
		mitarbeiterRepo.save(mitarbeiterDetail);
		response.redirect("/mitarbeiter?id="+mitarbeiterDetail.getId());
		return null;
	}
}


