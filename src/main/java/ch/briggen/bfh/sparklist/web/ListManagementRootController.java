package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.StakeholderlistRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

//WebController
// Liefert unter "/" die ganze Liste
 
public class ListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	StakeholderlistRepository stakeholderlistrepository = new StakeholderlistRepository();
	ProjektRepository projektrepository = new ProjektRepository();
	MitarbeiterRepository mitarbeiterrepository = new MitarbeiterRepository();
	MeilensteinRepository meilensteinrepository = new MeilensteinRepository();	
	

	
	//Liefert die Liste als Root-Seite "/" zurück 
	 	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		
		HashMap<String, Object> model = new HashMap<String, Object>();

		model.put("slist", stakeholderlistrepository.getAll());
		model.put("plist", projektrepository.getAll());
		model.put("mlist", mitarbeiterrepository.getAll());
		model.put("melist", meilensteinrepository.getAll());
		return new ModelAndView(model, "listTemplate");	
	}
}
