package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
//import ch.briggen.bfh.sparklist.domain.Projekt;
//import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class MitarbeiterEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterEditController.class);
	
	
	
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	//private ProjektRepository projektRepo = new ProjektRepository();
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /mitarbeiter für INSERT mit id " + idString);
			model.put("postAction", "/mitarbeiter/new");
			model.put("mitarbeiterDetail", new Mitarbeiter());

		}
		else
		{
			log.trace("GET /mitarbeiter für UPDATE mit id " + idString);
			model.put("postAction", "/mitarbeiter/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Mitarbeiter i = mitarbeiterRepo.getById(id);
			model.put("mitarbeiterDetail", i);
			//Projekt p = projektRepo.getById(id);
			//model.put("projektDetail", p);
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "mitarbeiterDetailTemplate");
	}
	
	
	
}


