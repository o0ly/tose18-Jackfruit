package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Stakeholderlistanzeigen;
import spark.Request;

class StakeholderlistanzeigenWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(StakeholderlistanzeigenWebHelper.class);
	
	public static Stakeholderlistanzeigen stakeholderlistanzeigenFromWeb(Request request)
	{
		return new Stakeholderlistanzeigen(
				Long.parseLong(request.queryParams("stakeholderlistanzeigenDetail.id")),
				request.queryParams("stakeholderlistanzeigenDetail.name"),
				request.queryParams("stakeholderlistanzeigenDetail.vorname"),
				request.queryParams("stakeholderlistanzeigenDetail.firma"),
				request.queryParams("stakeholderlistanzeigenDetail.rolle"),
				request.queryParams("stakeholderlistanzeigenDetail.wichtig"));
	}

}
