package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MeilensteinRepository;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
//import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
//import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjektEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektEditController.class);
	
	private ProjektRepository projektRepo = new ProjektRepository();
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	private MeilensteinRepository meilensteinRepo = new MeilensteinRepository();
	
	//Requesthandler zum Bearbeiten der Projekts
	//Liefert das Formular zum bearbeiten der einzelnen Fehler
	// Wenn der id Parameter 0 ist, wird beim submitte des Formulars ein neues Projekt erstellt (Aufruf von /projekt/new)
	//Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Projekt mit der übergebenen id upgedated (Aufruf /projekt/save)
	// @return gibt den Namen des zu verwendenden Templates zurück. Immer "projektDetailTemplate" .
	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("mlist", mitarbeiterRepo.getAll());
		model.put("melist", meilensteinRepo.getAll());
				
		if(null == idString)
		{
			log.trace("GET /projekt für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/projekt/new");
			model.put("projektDetail", new Projekt());

		}
		else
		{
			log.trace("GET /projekt für UPDATE mit id " + idString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/projekt/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projekt i = projektRepo.getById(id);
			model.put("projektDetail", i);
			//Mitarbeiter m = mitarbeiterRepo.getById(id);
			//model.put("mitarbeiterDetail", m);
		}
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projektDetailTemplate");
	}
	
	
	
}


