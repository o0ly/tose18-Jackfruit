package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

public class MitarbeiterDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterDeleteController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /mitarbeiter/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		mitarbeiterRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}


